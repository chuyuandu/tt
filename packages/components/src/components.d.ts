// 全局组件并没有任何属性提示,借助vscode中的volar给全局组件加上提示效果
import * as components from "./index";
declare module "@vue/runtime-core" {
  export interface GlobalComponents {
    VmcButton: typeof components.Button;
  }
}
export {};
