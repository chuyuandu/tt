import { series, parallel, src, dest, watch } from 'gulp';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import minimist from 'minimist';
import delPath from '../utils/delpath';
import run from '../utils/run';
import { pkgPath, componentPath } from '../utils/paths';

const sass = gulpSass(dartSass);

let knownOptions = {
  string: 'env',
  default: {
    key: true,
    env: process.env.NODE_ENV || 'production'
  }
};

let options = minimist(process.argv.slice(2), knownOptions);

//删除dist
export const removeDist = () => {
  return delPath(`${pkgPath}/visionmcui`);
};

//打包样式
export const buildStyle = () => {
  return src(`${componentPath}/src/**/style/**.scss`)
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(dest(`${pkgPath}/visionmcui/lib/src`))
    .pipe(dest(`${pkgPath}/visionmcui/es/src`));
};

// 复制重置element-plus颜色文件
export const buildElePlusColor = () => {
  return src(`${componentPath}/resetstyles/color.scss`)
    .pipe(dest(`${pkgPath}/visionmcui/es/resetstyles`))
    .pipe(dest(`${pkgPath}/visionmcui/lib/resetstyles`));
};

//打包重置element-ui样式
export const buildResetStyle = () => {
  return src([
    `${componentPath}/resetstyles/**.scss`,
    `!${componentPath}/resetstyles/color.scss`
  ])
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(dest(`${pkgPath}/visionmcui/lib/resetstyles`))
    .pipe(dest(`${pkgPath}/visionmcui/es/resetstyles`));
};

//打包组件
export const buildComponent = async () => {
  run('pnpm run build', componentPath);
};

// 当为开发模式时开启文件监听模式
if (options.env === 'development') {
  watch(`${componentPath}/src/**/style/**.scss`, buildStyle);
  watch(`${componentPath}/resetstyles/color.scss`, buildElePlusColor);
  watch(
    [
      `${componentPath}/resetstyles/**.scss`,
      `!${componentPath}/resetstyles/color.scss`
    ],
    buildResetStyle
  );

  watch(
    [`${componentPath}/src/**/**.vue`, `${componentPath}/src/**/**.ts`],
    series(
      async () => removeDist(),
      parallel(
        async () => buildStyle(),
        async () => buildResetStyle(),
        async () => buildElePlusColor(),
        async () => buildComponent()
      )
    )
  );
}

export default series(
  async () => removeDist(),
  parallel(
    async () => buildStyle(),
    async () => buildResetStyle(),
    async () => buildElePlusColor(),
    async () => buildComponent()
  )
);
