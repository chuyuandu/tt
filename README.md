# 简介

visionmc-ui为visionmc-ui 内部组件库，适用于vue3项目，包括自定义组件，修改element-plus组件样式

# 开发指南

## 相关技术栈

| 组件            | 版本     |
| :-------------- | :------- |
| vue             | 3.4.14   |
| vite            | 5.0.11   |
| typescript      | 5.2.2    |
| sass            | 1.70.0   |
| vite-plugin-dts | 3.7.1    |
| element-plus    | 2.5.1    |
| gulp            | 4.0.2    |
| gulp-sass       | 5.1.0    |
| prettier        | 3.2.4    |
| eslint          | 8.56.0   |
| minimist        | 1.2.8    |
| pnpm            | 8.6.12   |
| node            | v16.14.2 |

## 命令行解释

| 命令             | 说明                                 |
| :--------------- | :----------------------------------- |
| start:visionmcui | 开始运行编译编译并开启文件监听热更新 |
| build:visionmcui | 编译组件库，不会开启文件监听         |
| lint:script      | 运行代码检查并修复                   |
| build:pub        | 编译组件库并进行发布                 |
| pub:only         | 只执行发布命令                       |

## 文件夹解释

| 名称                            | 说明                                                                 |
| :------------------------------ | :------------------------------------------------------------------- |
| play                            | 预览项目，通过vite + vue3 搭建的组件效果预览项目                     |
| packages                        | pnpm 项目子包文件夹                                                  |
| packages/components             | 组件库项目源码                                                       |
| packages/visionmcui             | 组件库项目需要发布的文件，内包含package.json和README以及编译后的文件 |
| packages/utils                  | 工具包，暂时未启用                                                   |
| packages/components/resetstyles | 重写element-plus样式文件夹，所有相关的文件都在此文件下               |
| packages/components/script      | 组件库编译相关代码                                                   |
| packages/components/src         | 组件库自定义组件源码位置                                             |

## 开发流程

1. 拉取项目：`git clone https://gitee.com/coder-yl/visionmcui.git`
2. 拉取依赖: 进入项目,执行：`pnpm install`
3. 启动项目：终端执行：`pnpm start:visionmcui`
4. 启动预览项目：进入play目录执行：`pnpm dev`

### 开发一个自定义组件

1. `packages/components/src` 目录下新建一个组件目录,内部包含组件文件，style目录以及一个index.ts文件，index.ts文件内容大致如下，其主要目的是给此组件加上install，实现组件注册

```ts
import _Button from './button.vue';
import withInstall from '../../utils/withInstall';

export const Button = withInstall(_Button);
export default Button;
```

2. `packages/components/src/components.d.ts`文件下增加对应组件的类型声明
3. `packages/components/src/index.ts`文件导出对应组件

### 修改element-plus样式

1. `packages/components/resetstyles/`文件夹下增加对应样式文件
2. `packages/components/resetstyles/index.scss`导入对应文件

# 使用指南

## 安装依赖

```shell
pnpm install visionmcui
```

## 使用自定义内部组件

### 全量引入

```ts
import { createApp } from 'vue';
import App from './app.vue';

const app = createApp(App);
app.use(visionmcui);
app.mount('#app');
```

### 按需引入

直接在内部引入相关组件即可

```vue
<template>
  <p><Button type="primary" /></p>
</template>

<script setup lang="ts">
import { Button } from 'visionmcui';
</script>
```

### 使用element-plus组件样式

修改element-plus组件主题根据element-plus官方使用了sass变量方式，所以项目需要引入element-plus和sass

1. 安装依赖

```shell
pnpm install element-plus visionmcui
pnpm install sass -D
```

2. 使用

main.ts

```ts
import { createApp } from 'vue';
import App from './app.vue';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
// 导入重置element-plus样式文件
import 'visionmcui/es/resetstyles/index.css';
// 导入重置颜色文件,记住是scss类型
import 'visionmcui/es/resetstyles/color.scss';

const app = createApp(App);
app.use(visionmcui);
app.use(ElementPlus);
app.mount('#app');
```

app.vue

```vue
<template>
  <!-- visionmc-ui 顶部样式class,用于覆盖element-plus本身的样式 -->
  <div class="visionmc-ui-wapper"></div>
</template>
<script lang="ts" setup></script>
```
