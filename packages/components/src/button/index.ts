import _Button from './button.vue';
import withInstall from '../../utils/withInstall';

export const Button = withInstall(_Button);
export default Button;
