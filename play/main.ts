import { createApp } from 'vue';
import App from './app.vue';
import ElementPlus from 'element-plus';
import visionmcui from 'visionmcui';
import 'element-plus/dist/index.css';
// 导入重置element-plus样式文件
import 'visionmcui/es/resetstyles/index.css';
import 'visionmcui/es/resetstyles/color.scss';
import './common.scss';

const app = createApp(App);
app.use(visionmcui);
app.use(ElementPlus);
app.mount('#app');
