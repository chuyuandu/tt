import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import dts from 'vite-plugin-dts';
import DefineOptions from 'unplugin-vue-define-options/vite';
export default defineConfig({
  root: './',
  build: {
    //打包文件目录
    outDir: 'visionmcui',
    emptyOutDir: false,
    //压缩
    // minify: false,
    rollupOptions: {
      //忽略打包的文件
      external: ['vue', 'element-plus', '@element-plus/icons-vue', /\.scss/],
      input: ['index.ts'],
      output: [
        {
          //打包格式
          format: 'es',
          //打包后文件名
          entryFileNames: '[name].mjs',
          //让打包目录和我们目录对应
          preserveModules: true,
          exports: 'named',
          //配置打包根目录
          dir: '../visionmcui/es'
        },
        {
          //打包格式
          format: 'cjs',
          //打包后文件名
          entryFileNames: '[name].js',
          //让打包目录和我们目录对应
          preserveModules: true,
          exports: 'named',
          //配置打包根目录
          dir: '../visionmcui/lib'
        }
      ]
    },
    lib: {
      entry: './index.ts',
      name: 'visionmcui'
    }
  },
  plugins: [
    vue(),
    dts({
      entryRoot: 'src',
      outDir: ['../visionmcui/es/src', '../visionmcui/lib/src'],
      //指定使用的tsconfig.json为我们整个项目根目录下,如果不配置,你也可以在components下新建tsconfig.json
      tsconfigPath: '../../tsconfig.json',
      // 限制类型文件生成在 `outDir` 内
      strictOutput: false,
      // 去除某些文件
      exclude: [
        '../../node_modules/**',
        'script/**',
        '../../play/**',
        'vite.config.ts'
      ],
      // 是否将源码里的 .d.ts 文件复制到 `outDir`
      copyDtsFiles: true
    }),
    DefineOptions(),
    {
      name: 'style',
      generateBundle(config, bundle) {
        //这里可以获取打包后的文件目录以及代码code
        const keys = Object.keys(bundle);

        for (const key of keys) {
          const bundler: any = bundle[key as any];
          //rollup内置方法,将所有输出文件code中的.scss换成.css,因为我们当时没有打包scss文件

          this.emitFile({
            type: 'asset',
            fileName: key, //文件名名不变
            source: bundler.code.replace(/\.scss/g, '.css')
          });
        }
      }
    }
  ]
});
