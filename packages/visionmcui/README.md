# 简介

visionmc-ui为visionmc-ui 内部组件库，适用于vue3项目，包括自定义组件，修改element-plus组件样式

# 使用

## 安装依赖

```shell
pnpm install visionmcui
```

## 使用自定义内部组件

### 全量引入

```ts
import { createApp } from 'vue';
import App from './app.vue';

const app = createApp(App);
app.use(visionmcui);
app.mount('#app');
```

### 按需引入

直接在内部引入相关组件即可

```vue
<template>
  <p><Button type="primary" /></p>
</template>

<script setup lang="ts">
import { Button } from 'visionmcui';
</script>
```

### 使用element-plus组件样式

修改element-plus组件主题根据element-plus官方使用了sass变量方式，所以项目需要引入element-plus和sass

1. 安装依赖

```shell
pnpm install element-plus visionmcui
pnpm install sass -D
```

2. 使用

main.ts

```ts
import { createApp } from 'vue';
import App from './app.vue';
import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
// 导入重置element-plus样式文件
import 'visionmcui/es/resetstyles/index.css';
// 导入重置颜色文件,记住是scss类型
import 'visionmcui/es/resetstyles/color.scss';

const app = createApp(App);
app.use(visionmcui);
app.use(ElementPlus);
app.mount('#app');
```

app.vue

```vue
<template>
  <!-- visionmc-ui 顶部样式class,用于覆盖element-plus本身的样式 -->
  <div class="visionmc-ui-wapper"></div>
</template>
<script lang="ts" setup></script>
```
